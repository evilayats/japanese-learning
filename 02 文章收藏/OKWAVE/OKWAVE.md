## OKWAVE网站 https://okwave.jp/ 日文材料翻译练习

### OKWAVEは、あらゆる悩みや疑問を解決できるQ&Aサービスです

1. あらゆる：所有,一切

### このサービスは、人々が持つ優しい心をWEBの力で、より役立てられるかもしれないという着想から生まれました。
1. 着想（ちゃくそう）：构思

### 多くの利用者の中には、あなたの質問や悩みに回答してくれる人がきっといます。


### 新着質問（しんちゃくしつもん）
新问题

### 事業用電話を使わない選択ってあるのでしょうか？
不设立事业专用电话的选择是有的吧？

### 自分の考えとしては配達や一般客の予約を受け付けないので、客が店に電話をかける必要がないということですが、しかしクレームのため店に電話かけてくる場合や、仕入れ先取引先のことを考えると店に電話が無いっていけないことなのかと思い、一般客に対しては使わないですけど、必要なのでしょうか。
我的想法是不接受外送和一般客人的预约，所以客人不需要给店里打电话。但是考虑到为了投诉而给店里打电话，或者供应商、客户有事需要联系店里的情况，我觉得店里就不能没有电话。虽然电话不对一般客人使用，但是还是有必要（设置）的吧？
1. 配達（はいたつ）：投递
2. 一般客（いっぱんきゃく）：regular customer
3. 受け付ける：to accept
4. 仕入れ先（しいれさき）：供应商

### 法人登記の話？

1. 法人（ほうじん）：〈法〉法人
2. 登記（とうき）：〈法〉登记

### 例えばですが、バーチャルオフィスに登記してもいいんですよ。
1. バーチャル（virtual）：［形動］実体を伴わないさま。仮想的。疑似的。
2. 実体（じったい）：实体
3. 仮想（かそう）：假想,设想
4. 疑似（ぎじ）：疑似

### つまり店には電話はない！

### せいぜいバーチャルオフィスの人が受け答えしてくれる程度クレーム言われようが何しようが、それ自体がバリケードとして機能してくれますので。

1. せいぜい: 最多也……,充其量
2. バリケード（barricade）：路障, 街垒, 屏障


### わかりますよね？

### バーチャルオフィスの電話オペレーターはプロです。


### その程度のクレーマーの処置で業務を邪魔されるようなアホな事はしませんよ！

1. クレーマー：无脑投诉的人
2. アホ: = あほう【阿呆】傻子,浑人; 愚,蠢
3. 処置（しょち）：处理

### 考えても見て！

### バーチャルオフィスの人が、簡単に顧客を炎上されるような人たちだったら、ビジネスにならないでしょ＾＾ってこと、こっちは、面倒なので、弁護士いれてます。
1. 炎上（えんじょう）：起火,失火; 生气。

### 基本は、電話オペレーターが処理しますが、トラブルメーカーだと判断したら弁護士に対応させてますから。

### なので、質問者さんも、業務を邪魔されないように、鉄壁のガードで！がんばりましょ～！
1. 鉄壁（てっぺき）：铁壁
2. ガード（guard）：防御; 保护; 看守。

### ですです
