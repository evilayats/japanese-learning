### 泣く子を宥める
哄哭泣的孩子
1. 宥める（なだめる）：怒りや不満などをやわらげ静める。

### その必要性を改めて痛感した。
重新认识到了必要性。
1. 改めて（あらためて）：重新


### 足をとどめる。
止步。
1. とどめる（止める）：停止

### 昔の姿をとどめる。
保留原样不变
1. とどめる（止める）：保留

### この文書には、当時の生活の様子が克明に記録されている。
这份文件细致地记载了当时的生活情况。
1. こくめい（克明）：认真仔细，正直老实，不遗余力，一丝不苟

### きょう‐めい【共鳴】：他人の考えや行動などに心から同感すること。

### こうめい（公明）：［名・形動］公平で、不正や隠しだてがないこと。また、そのさま。公道，公正。
1. かくし‐だて【隠し立て】：［名］(スル)自分のした事・知っている事などを、人に知らせないようにすること。

### 殊更に（ことさらに）：特別に、わざわざ、などの意味の表現。


### 試合会場には緊迫した雰囲気が漂っていた。
比赛会场上弥漫着紧张的气氛。
1. 緊迫（きんぱく）：紧迫，紧急
2. 漂う（ただよう）：飘荡；弥漫


### 迫力に欠ける
迫力（はく‐りょく）：見る人や聞く人の心に強く迫る力。强烈逼近观众和听众内心的力量

 

### 会の運営を寄付金に依存する。
会的经营依赖于捐款。
1. きふ‐きん【寄付金】：組織・団体に贈った金銭。
2. 依存（いそん/いぞん）：依存，依赖。


### アルコール依存症
酒精依赖症
1. 依存症（いそん‐しょう/いぞんしょう）：依赖症

### ぎん‐が【銀河】：天の川。地球上から見たときの銀河系。
1. 天の川（あまのがわ）：银河

### てん‐かん【天漢】：あまのがわ。
1. あま【天】：【補説】 複合語を作ったり、「あまつ」「あまの」の形で体言にかかったりする場合に多く用いられる。→天(あま)つ →天(あま)の
2. 補説（ほせつ）：补充说明

### 政治的風土
政治的风气。
1. 風土（ふうど）：影响人类文化形成等的精神环境；风土。

### 風情のある庭
很有风情的院子。
1. 風情（ふぜい）：风情，情趣，风格。

### 趣のある庭。
1. 趣（おもむき）：风情，情趣


### 友人が困っているのに何もできず、とてももどかしかった。

1. もどかしい：令人着急，令人不耐烦，急不可待。（物事が思いどおりに行かなくて、歯痒い気分になること）
2. 歯痒い（はがゆい）：impatient；使干着急的


### 気が揉める
令人着急
1. 揉める（も・める）：いらいらする。很焦躁的。

### じれったい
令人着急



### 堅苦しいあいさつは抜きにする
去除死板的问候
1. 堅苦しい（かたくるしい）：限制过严；没有通融余地，死板，拘泥形式。

### あじけない世の中
无聊的人间
1. あじけない（味気ない）：乏味的，无聊的；无价值的，没有用的。
2. 近义词：「つまらない」「やるせない」


### 厚かましいお願いで恐縮です。（自谦）
1.  厚かましい（あつかましい）：厚脸皮、不知羞耻。近义词：「ずうずうしい」

### 仕組みを熟知している。
1. 熟知（じゅくち）：熟悉，知道的很详细。


### 機運が熟成する。
时机成熟
1. 熟成（じゅくせい）：成熟。


### 古代史の謎を探索する。「探る（さぐる）」
探索古代史的未解之谜
1. 探索（たんさく）：搜索罪犯的去向和罪状等。


### 犯人を探索する。「捜し求める」
搜索犯人的去向
1. 捜し求める（さがし‐もとめる）：目的の物を手に入れようと方々をさがす。


### 敵の動向を探知する。
探查敌人的动向
1. 探知（たんち）：侦察、探查隐藏的东西。


### 交渉がまとまりそうだったのに、余計なことを言ったせいで話がこじれてしまった。
眼看着谈判就要结束了，却因为说了多余的话，把事情复杂化了。
1. こじれる（拗れる）：（事）复杂化；（病）恶化；（性格）执拗，别扭。

### 風邪が拗れる。
感冒恶化了。
1. こじれる（拗れる）：（事）复杂化；（病）恶化；（性格）执拗，别扭。


### 気持ちが拗れる。
闹别扭了
1. こじれる（拗れる）：（事）复杂化；（病）恶化；（性格）执拗，别扭。

### ドアに凭れる。
依靠在门上
1. もたれる（凭れる）：倚靠；积食，不消化。

### 食べ過ぎで胃がもたれる。（胃もたれ（いもたれ）：名词，代表不消化的症状）
吃多了胃不消化。
1. もたれる（凭れる）：倚靠；积食，不消化。

### 酒色に溺れる。
沉迷于酒色
1. 酒色（しゅしょく）：wine and women
2. おぼれる（溺れる）：溺水，淹没；淹死；沉迷，迷恋。

### 長い間しゃがんでいたので足が痺れた。
因为长时间蹲着所以脚麻了
1. しゃがむ：蹲
2. しびれる（痺れる）：麻木；陶醉。

### ジャズ演奏に痺れる。（陶酔 とう‐すい）
陶醉于爵士乐的演奏
1. しびれる（痺れる）：麻木；陶醉。



