### ペンキがべったり（と）手につく。
油漆黏在了手上
1.  べったり：粘上，粘住；

### 母親にべったり（と）纏わり付く（まとわりつく）。
孩子非常地粘母亲
1. べったり：粘人，关系好；
2. 纏わり付く（まとわりつく）:粘人，纠缠

### べったり（と）座りこむ。
一屁股坐在地上
1. べったり：一屁股坐在地上
2. 座りこむ（すわりこむ）：to sit down (and refuse to move)


### 